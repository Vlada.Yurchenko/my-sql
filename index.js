const express = require('express');
const { employesRouter, departmentRouter, taskRouter } = require('./routers');
const app = express();
const connection = require('./db-connection');

app.use(express.json());

app.use('/employes', employesRouter);
app.use('/departments', departmentRouter);
app.use('/tasks', taskRouter);

app.use((err, req, res) => {
  res.status(500).send(err)
});

connection.connect(err => {
  if (err) {
    console.error('error connecting: ' + err.stack);
    process.exit(1); // eslint-disable-line no-undef
  }

  console.log('connected as id ' + connection.threadId);
  app.listen(3000, () => {
    console.log('Server is running!!!');
  });
});
