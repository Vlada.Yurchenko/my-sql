const connection = require('../db-connection');

const getDepartments = (req, res, next) => {
  if (!req.query.length) {
    connection.query('SELECT * FROM Departments', function (error, results) {
      if (error) return next(error);
      res.send(results);
    });

    return;
  }

  const { isActive } = req.query;

  if (isActive) {
    connection.query(`SELECT * FROM Departments WHERE isActive=${isActive}`, function (error, results) {
      if (error) return next(error);
      res.send(results);
    });
  }
}

const getDepartment = (req, res, next) => {
  const { departmentId } = req.params;

  connection.query(`SELECT * FROM Departments WHERE id=${departmentId};`, function (error, results) {
    if (error) return next(error);
    res.send(results);
  });
}

const createDepartment = (req, res, next) => {
  const { name, isActive } = req.body;
  const queryStr = `INSERT INTO Departments(name, isActive) VALUES ('${name}',${isActive});`

  connection.query(queryStr, function (error, results) {
    if (error) return next(error);
    res.send(results);
  });
}

const deleteDepartment = (req, res, next) => {
  const { departmentId } = req.params;

  connection.query(`DELETE FROM Departments WHERE id=${departmentId};`, function (error, results) {
    if (error) return next(error);
    res.send(results);
  });
}

module.exports = {
  getDepartments,
  getDepartment,
  createDepartment,
  deleteDepartment
}
