const connection = require('../db-connection');

const getEmployes = (req, res, next) => {
  if (!req.query.length) {
    connection.query('SELECT * FROM Employes', function (error, results) {
      if (error) return next(error);
      res.send(results);
    });

    return;
  }

  const { search, minYear, maxYear } = req.query;

  if (search) {
    connection.query(`SELECT * FROM Employes WHERE firstName='${search}'`, function (error, results) {
      if (error) return next(error);
      res.send(results);
    });
  }

  if (minYear && maxYear) {
    connection.query(`SELECT * FROM Employes WHERE age BETWEEN '${minYear}' AND '${maxYear}' `, function (error, results) {
      if (error) return next(error);
      res.send(results);
    });
  }
}

const getTotalSalary = (req, res, next) => {
  connection.query('SELECT SUM(salary) FROM Employes', function (error, results) {
    if (error) return next(error);
    res.send(results);
  });
}

const getEmployer = (req, res, next) => {
  const { userId } = req.params;

  connection.query(`SELECT * FROM Employes WHERE id=${userId};`, function (error, results) {
    if (error) return next(error)
    res.send(results);
  });
}

const createEmployer = (req, res, next) => {
  const { firstName, lastName, age, salary } = req.body;
  const queryStr = `INSERT INTO Employes(firstName, lastName, age, salary) 
    VALUES ('${firstName}', '${lastName}', ${age}, ${salary});`

  connection.query(queryStr, function (error, results) {
    if (error) return next(error);
    res.send(results);
  });
}

const deleteEmployer = (req, res, next) => {
  const { userId } = req.params;

  connection.query(`DELETE FROM Employes WHERE id=${userId};`, function (error, results) {
    if (error) return next(error);
    res.send(results);
  });
}

module.exports = {
  getEmployes,
  getTotalSalary,
  getEmployer,
  createEmployer,
  deleteEmployer
}