const connection = require('../db-connection');

const getTasks = (req, res, next) => {
  connection.query('SELECT * FROM Tasks', function (error, results) {
    if (error) return next(error);
    res.send(results);
  });
}

const getTask = (req, res, next) => {
  const { taskId } = req.params;

  connection.query(`SELECT * FROM Tasks WHERE id=${taskId};`, function (error, results) {
    if (error) return next(error);
    res.send(results);
  });
}

const createTask = (req, res, next) => {
  const { name, startDate, endDate } = req.body;
  const queryStr = `INSERT INTO Tasks(name, startDate, endDate) VALUES ('${name}','${startDate}', '${endDate}');`

  connection.query(queryStr, function (error, results) {
    if (error) return next(error);
    res.send(results);
  });
}

const deleteTask = (req, res, next) => {
  const { taskId } = req.params;

  connection.query(`DELETE FROM Tasks WHERE id=${taskId};`, function (error, results) {
    if (error) return next(error);
    res.send(results);
  });
}

module.exports = {
  getTasks,
  getTask,
  createTask,
  deleteTask
}
