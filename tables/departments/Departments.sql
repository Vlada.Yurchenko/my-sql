USE userDB;

CREATE TABLE Departments (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name CHAR(20) NOT NULL,
  isActive  BOOLEAN DEFAULT false,
  createdAt DATETIME DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO Departments(name)
VALUES ('Marketing');

INSERT INTO Departments(name, isActive)
VALUES ('Acounting', true);

INSERT INTO Departments(name, isActive)
VALUES ('Production', true);