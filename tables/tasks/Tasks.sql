USE userDB;

CREATE TABLE Tasks (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name CHAR(150) NOT NULL,
  startDate DATE,
  endDate  DATE,
  createdAt DATETIME DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO Tasks(name, startDate, endDate)
VALUES ('task-marketing', '2021-1-1',  '2021-2-1');

INSERT INTO Tasks(name, startDate, endDate)
VALUES ('task-acounting', '2021-1-10',  '2021-3-17');

INSERT INTO Tasks(name, startDate, endDate)
VALUES ('task-production', '2021-3-1',  '2021-5-20');