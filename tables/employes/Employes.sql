USE userDB;

CREATE TABLE Employes (
  id INT PRIMARY KEY AUTO_INCREMENT,
  firstName CHAR(20) NOT NULL,
  lastName CHAR(20) NOT NULL,
  age  INT NOT NULL,
  salary  INT NOT NULL,
  createdAt DATETIME DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO Employes(firstName, lastName, age, salary)
VALUES ('Kate', 'Middleton', 20, 5000);

INSERT INTO Employes(firstName, lastName, age, salary)
VALUES ('Jim', 'Kerry', 25, 144000);

INSERT INTO Employes(firstName, lastName, age, salary)
VALUES ('Ann', 'Hathaway', 25, 144000);