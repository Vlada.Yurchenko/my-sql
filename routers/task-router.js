const express = require('express');
const { taskController } = require('../controllers');
const taskRouter = express.Router();

taskRouter.get('/', taskController.getTasks);
taskRouter.get('/:taskId', taskController.getTask);
taskRouter.post('/', taskController.createTask);
taskRouter.delete('/:taskId', taskController.deleteTask);

module.exports = taskRouter;
