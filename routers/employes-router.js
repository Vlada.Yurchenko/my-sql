const express = require('express');
const { employesController } = require('../controllers');
const employesRouter = express.Router();

employesRouter.get('/', employesController.getEmployes);
employesRouter.get('/total-salary', employesController.getTotalSalary);
employesRouter.get('/:userId', employesController.getEmployer);
employesRouter.post('/', employesController.createEmployer);
employesRouter.delete('/:userId', employesController.deleteEmployer);

module.exports = employesRouter;
