const express = require('express');
const { departmentController } = require('../controllers');
const departmentRouter = express.Router();

departmentRouter.get('/', departmentController.getDepartments);
departmentRouter.get('/:departmentId', departmentController.getDepartment);
departmentRouter.post('/', departmentController.createDepartment);
departmentRouter.delete('/:departmentId', departmentController.deleteDepartment);

module.exports = departmentRouter;
